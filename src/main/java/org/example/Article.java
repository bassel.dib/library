package org.example;

import java.util.List;

public class Article {
    private String titleTestiflargerthantwentychar;
    private String author;
    private List<String> authors;
    private String dateOfPublication;
    private String Summery;
    private String content;
    private int likesCount;
    private int readCount;

    public Article() {
    }

    public Article(String aTitle, List<String> aAuther, String aDateOfPublication, String aSummery, String aContent){
        titleTestiflargerthantwentychar = aTitle;
        authors = aAuther;
        dateOfPublication = aDateOfPublication;
        Summery = aSummery;
        content = aContent;
        likesCount = 0;
        readCount = 0;
    }

    public void increaseArticleLikes (Article obj){
        obj.likesCount++;
    }
    public void increaseArticleRead(Article obj){
        this.readCount++;
    }
    @Override
    public String toString() {
        return "Article{" +
                "title='" + titleTestiflargerthantwentychar + '\'' +
                ", author='" + authors + '\'' +
                ", dateOfPublication=" + dateOfPublication +
                ", summery=" + Summery +
                ", content=" + content +
                '}';
    }
    public int getLikesCount() {
        return likesCount;
    }
    public int getReadCount() {
        return readCount;
    }
    public String getTitleTestiflargerthantwentychar() {
        return titleTestiflargerthantwentychar;
    }
    public String getAuthor() {
        return author;
    }
    public String getDateOfPublication() {
        return dateOfPublication;
    }
    public String getSummery() {
        return Summery;
    }
    public String getContent() {
        return content;
    }
}
