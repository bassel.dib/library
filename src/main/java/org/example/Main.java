package org.example;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Libraryc bib = new Libraryc();

        /* create and add books and Customers to the library. Articles can have more the one authors */
        Book buchA = new Book("Dragons of Eden","Carl Sagan","1977-05-13", "Speculations on the evolution of human intelligence", "1.cosmic calendar 2.gene and brain");
        Book buchB = new Book("how to Write a Thesis","Rowena Murray","2025-10-20", "Providing down-to-earth guidance to help students shape their theses", "1. academic writing 2.Goal Setting 3. Starting to write");
        Article artA = new Article("The hardest problem in computer science", Arrays.asList("Rumwick","Second Author"), "2016-07-02", "The problem is, of course, naming.", "Arrays, list and Pointers");
        Customer kundeA = new Customer("Max","the Address","max@email.com");
        Customer kundeB = new Customer("More","street","more@email.com");
        Customer kundeC = new Customer("maari"," next street","maariemail.com");

        bib.addBook(buchA);
        bib.addBook(buchB);
        bib.addArticle(artA);
        bib.addCustomer(kundeA);
        bib.addCustomer(kundeB);
        bib.addCustomer(kundeC);

        /* Customers like and read books and articles
           if many books have same number of likes all are returned
           if no books were liked null is returned */
        kundeA.likesBook(buchB);
        kundeA.likesBook(buchA);
        kundeB.likesBook(buchA);
        //kundeB.likesBook(buchB);
        //kundeA.likesArticle(artA);
        System.out.println("most liked Book(s): " + bib.mostLikedBooklist());
        System.out.println(bib.mostLikedArticlelist());

        /* print customers before and after sorting after likecount */
        //System.out.println(bib.getCustomers());
        bib.sortAfterLikes(bib.getCustomers());
        System.out.println("Customer sorted ascending; " + bib.getCustomers()); //likeCount or other Attributes can be showed by editing the toString method of Customer

        /* check methods of Validation Class. returns true or false respectively if a condition is met or not */
        System.out.println("Name Validation outcome is " + Validation.isValidName(kundeA.getName()));
        System.out.println("Title Validation outcome is " + Validation.isValidTitle(buchB.getTitle()));//lowercase letter
        System.out.println("Email Validation outcome is " + Validation.isValidEmail(kundeC.getEmail()));//email without @
        System.out.println("Summery Validation outcome is " + Validation.isValidSummery(buchA.getSummery()));
        System.out.println("Date Validation outcome is " + Validation.isValidPublicationDate(buchA.getDateOfPublication()));

        /* MetaValidation */
        System.out.println("Fields Validation outcome is " + MetaValidator.ValidateFieldsNames());//dateofpublication in Book and Summery in Article violate the conditions
        System.out.println("Methods Validation outcome is " + MetaValidator.ValidateMethodsNames());

    }
}