package org.example;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String name;
    private String adress; //different type?
    private String email;
    private List<Article> likedArticles;
    private List<Book> likedBooks;
    private List<Article> readArticles;
    private List<Book> readBooks;
    private int bookLikesCount;
    private int articleLikesCount;
    private int articleReadCount;
    private int bookReadCount;

    //Constructer
    public Customer(String name, String address, String email) {
        this.name = name;
        this.adress = address;
        this.email = email;
        likedArticles = new ArrayList<>(); //make a list for every customer
        likedBooks = new ArrayList<>();
        readArticles = new ArrayList<>();
        readBooks = new ArrayList<>();
        bookLikesCount = 0;
        bookReadCount = 0;
        articleReadCount = 0;
        articleLikesCount = 0;
    }
    /* make lists to add and store liked or read books and article and increase the number of likes/Read for that book */
    public void likesArticle(Article obj){
        likedArticles.add(obj);
        obj.increaseArticleLikes(obj);
        articleLikesCount++;
    }
    public void likesBook(Book obj){
        likedBooks.add(obj);
        obj.increaseBookLikes(obj);
        bookLikesCount++; //count of liked books per customer,, does it need to be static?, in this case it would be same num for all instances
    }
    public void readBook(Book obj){
        readBooks.add(obj);
        obj.increaseBookRead(obj);
        bookReadCount++;
    }
    public void readArticle(Article obj){
        readArticles.add(obj);
        obj.increaseArticleRead(obj);
        articleReadCount++;
    }

    /* return list with the like or read books and article from a customer instance */
    public List<Article> getLikedArticles() {
        return likedArticles;
    }

    public List<Book> getLikedBooks() {
        return likedBooks;
    }

    public List<Article> getReadArticles() {
        return readArticles;
    }

    public List<Book> getReadBooks() {
        return readBooks;
    }

    public int getBookLikesCount() {
        return bookLikesCount;
    }

    public int getArticleLikesCount() {
        return articleLikesCount;
    }

    public int getArticleReadCount() {
        return articleReadCount;
    }

    public int getBookReadCount() {
        return bookReadCount;
    }

    @Override
    public String toString() {
        return "Customer{" +
                " name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", email='" + email + '}';//+ '\'' +
               // ", likedArticles=" + likedArticles +
               // ", likedBooks=" + likedBooks +
               //", readArticles=" + readArticles +
                //", readBooks=" + readBooks +
                //", bookLikesCount=" + bookLikesCount +
                //'}';
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getEmail() {
        return email;
    }
}
