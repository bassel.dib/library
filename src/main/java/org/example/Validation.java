package org.example;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class Validation {

    /* Validate customer and author names starting with a capital letter and not empty */
    public static boolean isValidName(String name){ //static => call method without making objects
            return name != null && !name.isEmpty() && Character.isUpperCase(name.charAt(0));

    }
    /* check valid Title by upperCase and String length */
    public static boolean isValidTitle(String title){
        return title != null && !title.isEmpty() && title.length() <= 300 & Character.isUpperCase(title.charAt(0));
    }
    /* check if summery is not Null (still can be empty) and length does not exceed 2000 */
    public static boolean isValidSummery(String summery){
        return summery != null && summery.length() <= 2000;
    }
    /* Validate if the email contains @ */
    public static boolean isValidEmail(String email){
        return email != null && email.contains("@");
    }

    /* Validate the date of publication is in the past, and has the right format yyyy-mm-dd */
    public static boolean isValidPublicationDate(String publicationDate) {
        try{
        LocalDate currentDate = LocalDate.now();
        LocalDate date = LocalDate.parse(publicationDate);
        return date.isBefore(currentDate);
        } catch (DateTimeParseException e) {
            System.out.println("invalid date format" + publicationDate);
            return false;
        }
    }
}
