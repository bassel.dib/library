package org.example;

public class Book {
    private String title;
    private String author;
    private String dateOfPublicationtesttestest;
    private String summery;
    private String content;
    public int likeCount;
    public int readCount;
    public Book(){
    }
    public Book(String aTitle, String aAuther, String aDateOfPublication, String aSummery, String aContent){
        title = aTitle;
        author = aAuther;
        dateOfPublicationtesttestest = aDateOfPublication;
        summery = aSummery;
        content = aContent;
        likeCount = 0;
        readCount = 0;
        //count++; //number of created books, out also Booklist.size() in library has it
    }

    /* to add one more like/read to a single book instance from Customer class */
    public void increaseBookLikes(Book obj){
        obj.likeCount++;
    }
    public void increaseBookRead(Book obj){
        obj.readCount++;
    }

    @Override
    public String toString() {
        return "Book{ " +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", dateOfPublication='" + dateOfPublicationtesttestest + '\'' +
                ", summery='" + summery + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
    public int getLikeCount(){
        return likeCount;
    }
    public int getReadCount() {
        return readCount;
    }
    public String getTitle() {
        return title;
    }
    public String getAuthor() {
        return author;
    }
    public String getDateOfPublication() {
        return dateOfPublicationtesttestest;
    }
    public String getSummery() {
        return summery;
    }
    public String getContent() {
        return content;
    }
}
