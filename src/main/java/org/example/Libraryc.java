package org.example;

import java.util.List;
import java.util.ArrayList;

public class Libraryc {

    private List<Article> articles; // should it be final?
    private List<Book> books;
    private List<Customer> customers;

    public Libraryc() {
        this.articles = new ArrayList<>();
        this.books = new ArrayList<>();
        this.customers = new ArrayList<>();
    }

    /* Methods to add articles, books and customers to the library */
    public void addArticle(Article article) {
        articles.add(article);
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    /* iterate through the list of Books/articles and return the most liked one (instance) */
    public Book mostLikedBook(){
        List<Book> allBooks; // could not pass allBooks as an argument for the method -> lambda?how?
        allBooks = getBooks();
        int temp = allBooks.get(0).getLikeCount();
        int index = 0; // assign index of most liked book
            for (int i = 0; i < allBooks.size(); ++i) {
                if(temp < allBooks.get(i).getLikeCount() ){ //&& temp != 0){
                   temp = allBooks.get(i).getLikeCount();
                   index = i;
                }
            }
            if (temp ==0)
                return null;// to avoid having most liked book (passed to mostlikedbooklist) with no likes returned
            return allBooks.get(index);
    }
    /* compare if like's count equals the likes of the most liked book, add it to mostLikedBookslist
       return list with all books with highest like count books */
    public List<Book> mostLikedBooklist(){
        List<Book> allBooks;
        List<Book> allMostLikedBooks = new ArrayList<>();
        allBooks = getBooks();
        Book temp;
        temp = mostLikedBook();
        if(temp == null) {
            //throw new NullPointerException("Likes count is null for the article"); //breaks the program
            System.out.print("No book was liked in the library therefore ");
            return null;
        }
        //allMostLikedBooks.add(temp); // add the most liked book for the returned list,, not needed because mostLikedBook is already an element of allBooks
        for(int i=0; i< allBooks.size();i++){
            if(allBooks.get(i).getLikeCount() == temp.getLikeCount()){
                allMostLikedBooks.add(allBooks.get(i));
            }
        }
        return allMostLikedBooks;
    }
    /* Insertion sort to change list's order of Customer after number of likes ascending */
    public void sortAfterLikes(List<Customer> array) {
        int i, j;
        for (i = 1; i < array.size(); i++) {
            Customer tmp = array.get(i);
            j = i;
            while ((j > 0) && (array.get(j - 1).getBookLikesCount() > tmp.getBookLikesCount())) {
                array.set(j, array.get(j - 1));
                j--;
            }
            array.set(j, tmp);
        }
    }
    public void sortAfterRead(List<Customer> array) {
        int i, j;
        for (i = 1; i < array.size(); i++) {
            Customer tmp = array.get(i);
            j = i;
            while ((j > 0) && (array.get(j - 1).getArticleReadCount() > tmp.getArticleReadCount())) {
                array.set(j, array.get(j - 1));
                j--;
            }
            array.set(j, tmp);
        }
    }
    @Override
    public String toString() {
        return "Libraryc{" +
                "articles=" + articles +
                ", books=" + books +
                ", customers=" + customers +
                '}';
    }
    public List<Book> getBooks() {
        return books;
    }
    public List<Article> getArticles() {
        return articles;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    /*same methods as for books */
    public Article mostLikedArticle(){
        List<Article> allArticles;
        allArticles = getArticles();
        int temp = getArticles().get(0).getLikesCount();
        int index = 0;
        for (int i = 0; i < getArticles().size(); ++i) {
            if(temp < getArticles().get(i).getLikesCount()){
                temp = allArticles.get(i).getLikesCount();
                index = i;
            }
        }
        if (temp ==0)
            return null;
        return allArticles.get(index);
    }
    public List<Article> mostLikedArticlelist(){
        List<Article> allArticles;
        allArticles = getArticles();
        Article temp;
        temp = mostLikedArticle();
        List<Article> allMostLikedArticles = new ArrayList<>();
        if(temp == null) {
            //throw new NullPointerException("Likes count is null for the article"); //breaks the program
            System.out.print("No Article was liked in the library therefore ");
            return null;
        }
        for(int i=0; i< allArticles.size();i++){
            if(allArticles.get(i).getLikesCount() == temp.getLikesCount()){
                allMostLikedArticles.add(allArticles.get(i));
            }
        }
        return allMostLikedArticles;
    }
}

