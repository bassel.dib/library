package org.example;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

public class MetaValidator {

    public MetaValidator(){

    }
    /* get Field names from all the classes, concatenate them to one array and iterate
     to check if a name violate the 20 rule or starts with uppercase-> return false */
    public static boolean ValidateFieldsNames(){

        Book book = new Book();
        Article article = new Article();
        boolean checker = false; // checker of violation
        Field[] fieldsBook= book.getClass().getDeclaredFields();
        Field[] fieldsArticle= article.getClass().getDeclaredFields();

        Field[] both = Stream.concat(Arrays.stream(fieldsBook), // concatenate Fields of both classes
                        Arrays.stream(fieldsArticle))
                .toArray(Field[]::new);

        for(Field f : both) {
            if(f.getName().length() > 20 || Character.isUpperCase(f.getName().charAt(0))) {
                System.out.println("Field \"" + f.getName() +"\" has more than 20 characters or dose not start with lower-case");
                checker = true;
            }
        }
        if(checker)
            return false;
        return true;
    }

    /* check if Methods start with lowercase or have more than 40 characters like implementation above */
    public static boolean ValidateMethodsNames(){

        Book book = new Book();
        Article article = new Article();
        boolean checker = false;
        Method[] methodsBook= book.getClass().getDeclaredMethods();
        Method[] methodsArticle= article.getClass().getDeclaredMethods();

        Method[] both = Stream.concat(Arrays.stream(methodsBook),
                        Arrays.stream(methodsArticle))
                .toArray(Method[]::new);

        for(Method f : both) {
            if(f.getName().length() > 40 || Character.isUpperCase(f.getName().charAt(0))) {
                System.out.println("Method \"" + f.getName() +"\" has more than 40 characters or dose not start with lower-case");
                checker = true;
            }
        }
        if(checker)
            return false;
        return true;
    }

}